<?php
require_once('car.php');
require_once('uberX.php');
require_once('uberPool.php');
require_once('uberVan.php');
require_once('account.php');

$uberX = new UberX("QWE123", new Account("Ray Blondell", "25306904"), "Chevrolet", "Spark");
$uberX->setPassenger(4);
$uberX->printDataCar();

$uberPool = new UberPool("QWE123", new Account("Vicky García", "25306904"), "Dodge", "Neon");
$uberX->setPassenger(4);
$uberPool->printDataCar();

$uberVan = new UberVan("OJL395", new Account("Ray Blondell", "AND456"), "Nissan", "Versa");
$uberVan->setPassenger(6);
$uberVan->printDataCar();
?>