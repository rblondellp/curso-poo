
class Main {
    public static void main(String[] args) {
        System.out.println("Hola mundo");
        UberX uberX = new UberX("AMQ123", new Account("Andres Herrera", "AND123"), "Chevrolet", "Sonic");
        uberX.setPassenger(4);
        uberX.printDataCar();

        UberVan uberVan = new UberVan("QWE123", new Account("Ray Blondell", "ASD456"));
        uberVan.setPassenger(6);
        uberVan.printDataCar();
    }
}