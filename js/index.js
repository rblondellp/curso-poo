var car = new Car("34r234", new Account("Ray blondell", "25306904"));
car.passengers = 4;
car.printDataCar();

var uberX = new UberX(
    "34r234",
    new Account("Ray blondell", "25306904"),
    "Chevrolet",
    "Spark"
);
uberX.passengers = 4;
uberX.printDataCar();
